/* 
Project Name : Alien Registration Programme
Project language : JAVA
Proect Author Name : Shubham Mittal
Project created on : Sun, 28th September 2014
Project Description : To register aliens on earth and generate PDF/ plaintext document according to
user's needs.
*/

Driver package : com.registration.driver
Driver Class : RegistrationMenu.java (main class)

 How to run the console application:
1. Compile the main class using Java compiler.
2. Follow the registration steps.
3. File will be generated in the source folder. 

How to extend the code to add new format.
1. Check for maximumSupportedFormats (in FormatSupported Class in com.registration.printer package), increment the variable if you have exceeded the existing limt of supporting formats.
2. Add the class of the required format type in com.registration.printer package. 
3. In the added class of XYZ format add the following code snippet:
		/*******************************/
private static int status = 0;	
    static
    {	if(status == 0 )
    	{
    		
    		FormatSupported.setCount(FormatSupported.getCount() + 1 );
    		FormatSupported.setFormats( "XYZ" );
    		status = 1;
    	}
    	
    }	

4. Add the object of your newly created class in menuInitializer function(in FormatSupported Class in com.registration.printer package) as new nameofclass(); in order of of the choices you want in menu displayed.

5. Open the class printDetails in the package com.registration.printer and add your functioncall of the previously created class to print the details in the new format.
	5.1 Refer the following code snippet:
			/***************************/
				driver program
				
				1. PDF
				2. txt
				3. xyz(added by you)
			/***************************/
 
		else if ( type == 3 ) {
			xyzGenerator xyzGenerator = new xyzGenerator();
			this.setMessage(xyzGenerator.detailsReaderxyz(key, registrationdatabase));

		}
	
6. For function calling please refer to the syntax functionname(object registrationdata, string registration key, int type)


Limitations and Scope:
Database has not been included.
With inclusion of database of your choice(can be done using jpa, or jdbc, etc), the above project can serve as a registration server with little modification in the implemented datastructure.



Refer to the snapshots for more information or kindly write to me at shubham9910103545@gmail.com