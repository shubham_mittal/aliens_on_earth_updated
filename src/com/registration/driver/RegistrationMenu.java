package com.registration.driver;

import java.io.IOException;
import java.util.Date;

import com.registration.data.RegistrationInfo;
import com.registration.datastore.RegistrationDatabase;
import com.registration.exception.InvalidInputException;
import com.registration.input.DataInput;
import com.registration.printer.FormatSupported;
import com.registration.printer.PrintDetails;

public class RegistrationMenu {
	DataInput dataInput = new DataInput();
	PrintDetails printDetails = new PrintDetails();
	private String registrationId;
	private String status; // Registration status
	private String message; // File creation status.
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void defaultStart() {
		Date date = new Date();
		System.out.println( "\t\t\t\t\t" + date.toString() );
		System.out.println( "\t\tRegistration Portal" );
		System.out.println( "\t__________________________________" );
		System.out.println();
	}

	public void defaultPrint() {
		System.out.println( "Please Enter the type of Output file " );
		new FormatSupported().menuInitializer();
		
		for( int i = 0; i < FormatSupported.getCount(); i++ )
		{
			System.out.println( (i+1) + "." + FormatSupported.getFormats( i+1 ));
		}

	}

	public void showHelp() throws IOException, InvalidInputException {
		System.out
				.println( "This is a platform for Space Creatures Registration" );
		System.out
				.println( "Please Map the Boarind Id (already issued to creature at time of landing) as the Registration Id" );
		System.out.println( "1. Back " );
		if (Integer.parseInt( dataInput.dataScanner()) == 1 ) {
			menuDetails();
		}
	}

	public void newRegistration() throws NumberFormatException, IOException,
			InvalidInputException {

		RegistrationInfo registarionInfo = new RegistrationInfo();
		RegistrationDatabase registrationDatabase = new RegistrationDatabase();
		RegistrationMenu registrationMenu = new RegistrationMenu();
		defaultStart();

		System.out.println( "Registration Id : " );
		registrationMenu.setRegistrationId( dataInput.dataScanner() );
		System.out.println( "Code Name : " );
		registarionInfo.setCodeName( dataInput.dataScanner() );
		System.out.println( "Blood Color : " );
		registarionInfo.setBloodColor( dataInput.dataScanner() );
		System.out.println( "Number Of Antennas Creature is Carrying : " );
		boolean flag = false;
		while ( !flag ) {
			try {
				registarionInfo.setAntennasCount( Integer.parseInt(dataInput
						.dataScanner()) );
				flag = true;
			} catch ( Exception e ) {
				System.out.println( "Please Enter Input in number format" );
				flag = false;
			}
		}

		System.out.println( "Number Of Legs Creature Is Carrying : " );
		flag = false;
		while ( !flag ) {
			try {
				registarionInfo.setLegsCount( Integer.parseInt(dataInput
						.dataScanner()) );
				flag = true;
			} catch ( Exception e ) {
				System.out.println( "Please Enter Input in number format" );
				flag = false;
			}
		}

		System.out.println( "Home Planet : " );
		registarionInfo.setHomePlanet( dataInput.dataScanner() );
		System.out.println( "Duration Of Stay" );
		registarionInfo.setDurationOfStay( dataInput.dataScanner() );

		setStatus( registrationDatabase.registeredRecords(
				registrationMenu.getRegistrationId(), registarionInfo) );

		if ( getStatus().equals("Registration Successfull") ) {
			System.out.println( getStatus() );
			defaultPrint();

			flag = false;
			while ( !flag ) {
				try {
					Integer fileType = -1;
					while ( fileType <= 0 || fileType > FormatSupported.getCount() ) {
						fileType = Integer.parseInt( dataInput.dataScanner() );
						if (fileType <= 0 || fileType > FormatSupported.getCount() )
							System.out
									.println( "Invalid choice,Please enter again from above choices" );
					}
					registrationMenu.setMessage( printDetails.printTypeSelector(
							registrationMenu.getRegistrationId(), fileType,
							registrationDatabase) );
					flag = true;
				} catch (Exception e) {
					System.out.println( "Please Enter Input in number format" );
					flag = false;
				}
			}
			System.out.println( registrationMenu.getMessage() );
			menuDetails();
		} else {
			System.out.println(getStatus());
			menuDetails();
		}

	}

	public void existingRegistration() throws NumberFormatException,
			IOException, InvalidInputException {
		RegistrationMenu registrationMenu = new RegistrationMenu();

		defaultStart();

		System.out.println( "Registration Id : " );
		registrationMenu.setRegistrationId(dataInput.dataScanner());
		defaultPrint();
		boolean flag = false;
		while( !flag ){
			try {
				Integer fileType = -1;
				while ( fileType <= 0 || fileType > FormatSupported.getCount() ) {
					fileType = Integer.parseInt( dataInput.dataScanner() );
					if ( fileType <= 0 || fileType > FormatSupported.getCount() )
						System.out
								.println( "Invalid choice,Please enter again from above choices" );
				}
				
				registrationMenu.setMessage( printDetails.printExisting( fileType,
						registrationMenu.getRegistrationId() ) );
				flag = true;
			} catch (Exception e) {
				System.out.println( "Please Enter Input in number format" );
				flag = false;
			}
		}
		System.out.println(registrationMenu.getMessage());
		menuDetails();
	}

	public void menuDetails() throws IOException, InvalidInputException,
			NumberFormatException {

		defaultStart();
		System.out.println( "1. New Registration" );
		System.out.println( "2. Print Last Registration Details Again" );
		System.out.println( "3. Exit" );
		System.out.println( "4. Help" );

		int response;
		try {
			response = Integer.parseInt(dataInput.dataScanner());
		} catch (NumberFormatException e) {
			throw e;
		}

		if ( response == 1 ) {
			newRegistration();

		}

		if ( response == 2 ) {
			{
				existingRegistration();

			}
		}

		if ( response == 3 ) {
			System.exit(0);

		}

		if ( response == 4 ) {
			showHelp();

		} else {
			throw new InvalidInputException();
		}

	}

	public static void main(String[] args) throws IOException {
		RegistrationMenu registrationMenu = new RegistrationMenu();

		while (true) {

			try {
				registrationMenu.menuDetails();
			} catch (InvalidInputException e) {
				System.out.println(e.getMessage());
			} catch (IOException e) {
				System.out.println( "Please Enter Valid Input!!" );
			} catch (NumberFormatException e) {
				System.out.println( "Please Enter Valid Input!!" );
			} catch (Exception e) {
				System.out
						.println( "Unexpected error occurred, Please enter values again." );
			}

		}
	}

}
