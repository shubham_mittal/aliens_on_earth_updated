package com.registration.exception;

@SuppressWarnings("serial")
public class InvalidInputException extends Exception {

	public InvalidInputException() {
		super( "Invalid Input, Please Enter Again" );
	}

}
