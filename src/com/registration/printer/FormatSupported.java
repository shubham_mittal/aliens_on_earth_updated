package com.registration.printer;

public class FormatSupported {

	private static int count = 0;
	private static final int maximumSupportedFormats = 100; // update the value in case more than 100 formats are supported
	private static String formats [] = new String [maximumSupportedFormats];
	public static int getCount() {
		return count;
	}
	public static void setCount(int count) {
		FormatSupported.count = count;
	}
	public static String getFormats( int index) {
		return formats[index];
	}
	public static void setFormats(String formats) {
		
		FormatSupported.formats[count] = formats;
	}
	
	public void menuInitializer()
	{
		new PdfGenerator();
		new TxtGenerator();
	}
	
	

}
