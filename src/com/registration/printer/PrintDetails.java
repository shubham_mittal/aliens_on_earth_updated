package com.registration.printer;

import java.io.IOException;
import java.util.HashMap;


//import com.registration.data.RegistrationInfo;
import com.registration.datastore.RegistrationDatabase;
import com.registration.driver.RegistrationMenu;
import com.registration.exception.InvalidInputException;

public class PrintDetails {
	RegistrationDatabase registrationDatabase = null;
	RegistrationMenu registrationMenu = null;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String printTypeSelector(String key, int type,
			RegistrationDatabase registrationdatabase) throws IOException,
			InvalidInputException {
		registrationDatabase = registrationdatabase;

		if ( type == 1 ) {
			PdfGenerator pdfGenerator = new PdfGenerator();
			this.setMessage(pdfGenerator.detailsReaderPdf(key, registrationdatabase));

		}

		else if ( type == 2 ) {
			TxtGenerator txtGenerator = new TxtGenerator();
			this.setMessage(txtGenerator.detailsReaderTxt(key, registrationdatabase));

		}
		return this.getMessage();

	}

	public String printExisting(int type, String key) throws IOException,
			InvalidInputException {

		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		if( registrationDatabase == null ){
			this.setMessage( "No previous Registrations found!!" );
			return this.getMessage();
		}
		hashMap = registrationDatabase.getHashmap();
		if ( hashMap.containsKey(key) ) {
			if ( type == 1 ) {
				PdfGenerator pdfGenerator = new PdfGenerator();
				this.setMessage(pdfGenerator.detailsReaderPdf(key, registrationDatabase));
			}

			else if ( type == 2 ) {
				TxtGenerator txtGenerator = new TxtGenerator();
				this.setMessage(txtGenerator.detailsReaderTxt(key, registrationDatabase));

			} }else {
				this.setMessage( "Registration Id does not Exist" );
			}
		return this.getMessage();
		}
	}
